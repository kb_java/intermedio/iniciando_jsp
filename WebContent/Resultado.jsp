<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Mostrar Resultado</title>
</head>
<body>
<h1>MOSTRANDO RESULTADO</h1>
<%
	String prod = request.getParameter("selectProd");
	int pre = Integer.parseInt(request.getParameter("txtPrecio"));
	int cantidad = Integer.parseInt(request.getParameter("txtCantidad"));
	
	double subtotal = pre * cantidad;
	double igv = subtotal * 0.18;
	double total = subtotal + igv;	
%>

Producto: <%=prod%><br>
Precio: <%=pre %><br>
Cantidad = <%=cantidad %><br>
SubTotal = <%=subtotal %><br>
Igv = <%=igv %><br>
Total = <%=total %><br>

<a href="Index.jsp">Retornar</a>




</body>
</html>